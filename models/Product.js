const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	sku : {
		type: String,
		required : [true, "Product name is required"]
	},
	description : {
		type: String, 
		required: [true, "Descrption is required"]
	},
	img: {
		type: String,
		required: [true, "Image is required."]
	},
	origPrice : {
		type: Number,
		required : [true, "Price is required"]
	},
	percentVat: {
		type: Number
	},
	srp: {
		type: Number
	},
	inventory: {
		type: Number,
		default: 0
	},
	isActive : {
		type : Boolean,
		default : true
	},
	onSale: {
		type: Boolean,
		default: false
	},
	createdOn : {
		type : Date,
		default : new Date()
	}
});

module.exports = mongoose.model("Product", productSchema);