// [SECTION] Dependencies
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// [SECTION] Routes

// Route for adding a product
router.post("/add", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data)
		.then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts()
		.then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/active", (req, res) => {
	productController.getAllActiveProduct()
		.then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all on sale products
router.get("/sale", (req, res) => {
	productController.getOnSale()
		.then(resultFromController => res.send(resultFromController));
})

// Route for retrieving products under the client's budget
router.get("/budget", (req, res) => {
	const maxPrice = req.query.maxPrice;

	productController.getOnBudget(maxPrice)
		.then((resultFromController) => res.send(resultFromController));
})

// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	productController.getSingleProduct(req.params)
		.then(resultFromController => res.send(resultFromController));
});

// Route for update product detail
router.put("/:productId/update", auth.verify, (req,res) => {

	const data = {
		productId : req.params.productId,
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data)
		.then(resultFromController => res.send(resultFromController));
});

// Route for archiving product
router.patch("/:productId/archive", auth.verify, (req,res) => {

	const data = {
		productId : req.params.productId,
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;